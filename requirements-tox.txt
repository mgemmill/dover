coverage==4.5.1
docopt==0.6.2
flake8==3.5.0
mccabe==0.6.1
pylint==1.8.2
pytest==3.4.1
pytest-cov==2.5.1
toml==0.10.0
