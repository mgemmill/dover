dover v0.5.1
===================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



|version-badge| |codacy-coverage| |codacy-badge|


A commandline utility for incrementing your project version numbers.


Installation
^^^^^^^^^^^^

.. code-block:: text
    
    ... pip install dover


What does it do?
^^^^^^^^^^^^^^^^

When ``dover`` is run from the root directory of your project, it does the 
following:

    1. looks for a configuration file (``.dover``, ``.dover.ini``, ``setup.cfg``, ``pyproject.toml``)
    2. reads any ``dover`` configuration line in this format:

       .. code-block:: text
            
           [dover:file:relatvie/file.pth]

    Or in the case of ``pyproject.toml``:

       .. code-block:: text

           [tool.dover]
           versioned_files = ["pyproject.toml", "dover/cli.py"]


    3. searches the configured file references for "version" strings
    4. validates all version strings across all configured files.
    5. displays and/or increments the version strings based upon 
       cli options. 

Usage
^^^^^

.. code-block:: text 
    
    ... dover --help

    dover v0.5.1

    dover is a commandline utility for
    tracking and incrementing your
    project version numbers.

    Usage:
      dover [--list] [--debug] [--format=<fmt>]
      dover increment ((--major|--minor|--patch)
                       [--dev|--alpha|--beta|--rc] |
                       [--major|--minor|--patch]
                       (--dev|--alpha|--beta|--rc) | --release)
                       [--apply] [--debug] [--no-list] [--format=<fmt>]

    Options:
      -M --major      Update major version segment.
      -m --minor      Update minor version segment.
      -p --patch      Update patch version segment.
      -d --dev        Update dev version segment.
      -a --alpha      Update alpha pre-release segment.
      -b --beta       Update beta pre-release segment.
      -r --rc         Update release candidate segment.
      -R --release    Clear pre-release version.
      -x --no-list    Do not list files.
      --format=<fmt>  Apply format string.
      --debug         Print full exception info.
      -h --help       Display this help message
      --version       Display dover version.


Basics
^^^^^^

``dover`` will look for either a **.dover**,
**setup.cfg**, or **pyproject.toml** file within the current directory.

Files that are to be tracked by ``dover`` should be listed in the
following format:

.. code:: ini

    [dover:file:relative-file-path.ext]

Example:

.. code:: ini

    [dover:file:setup.py]
    [dover:file:setup.cfg]
    [dover:file:dover/cli.py]


For projects using the **pyproject.toml** the format will be:

.. code:: ini

   [tool.dover]
   versioned_files = ["pyproject.toml", "dover/cli.py"]

Fetch Current Project Version
.............................

Calling ``dover`` without any arguments returns the **current** version
number of the project.

.. code::

    ... dover
    0.1.0

This is useful for capturing your project version for use with other
commandline utilities.


Currently Tracked File Status
.............................

Calling ``dover`` with the ``--list`` option, prints the project version
and the list of all files and version strings being tracked, and the line
they appear on:

.. code::

    ... dover --list
    Current Version: 0.1.0
    Files:
        setup.py     0005 (__version__ = '0.1.0')
        setup.cfg    0002 (version = 0.1.0)
        dover/cli.py 0025 (__version__ = '0.1.0')


Reviewing Version Increment Changes
...................................

Calling ``dover increment`` with one the the segment options 
(e.g. ``--minor``), will print a listing of the propsed version 
change and the files that will be effected:

.. code::

    ... dover increment --minor
    Current Version: 0.1.0
    New Version:     0.2.0
    Files:
        setup.py      (0.1.0 -> 0.2.0)
        setup.cfg     (0.1.0 -> 0.2.0)
        dover/cli.py  (0.1.0 -> 0.2.0)


.. attention:: Only the use of the **--apply** option will perform a update to your files.


Applying Version Increment Changes
..................................

To save the change make the same call with the ``--apply`` option:

.. code:: bash

    ... dover increment --minor --apply
    Current Version: 0.1.0
    New Version:     0.2.0
    Files:
        setup.py      (0.1.0 -> 0.2.0)
        setup.cfg     (0.1.0 -> 0.2.0)
        dover/cli.py  (0.1.0 -> 0.2.0)
    Version updates applied.


Pre-Release Options
^^^^^^^^^^^^^^^^^^^

Applying a pre-release option (--dev, --alpha, --beta or --rc), simply appends
the pre-release to the current version:

.. code:: text
    
    ... dover increment --alpha --no-list
    Current Version: 0.1.0
    New Version:     0.1.0-alpha

.. tip:: the **--no-list** option suppresses listing the files.

Applying a pre-release option to an existing pre-release of the same name
increments the pre-release:

.. code:: text
    
    ... dover increment --alpha --no-list
    Current Version: 0.1.0
    New Version:     0.1.0-alpha.1

Applying a pre-release option with a segment option, increments the segment
and appends the pre-relase value:

.. code:: text
    
    ... dover increment --minor --alpha --no-list
    Current Version: 0.1.0
    New Version:     0.2.0-alpha


Use the ``--release`` option to move from a pre-release version to the production version:

.. code:: text
    
    ... dover increment  --release --no-list
    Current Version: 0.4.0-dev
    New Version:     0.4.0


Version Formats
^^^^^^^^^^^^^^^

The default version format `dover` uses is:

``major.minor[.patch][-(dev|alpha|beta|rc)[.version]]``

The output format can be controlled with the ``--format`` option with these options:

``00[0][(.|-)](r|R)[(.|-)][0]``

+----------------------+------------------+---------------------------------------+
| Segment              |  Format Value    |  Note                                 |
+======================+==================+=======================================+
| Major                |  0               |                                       | 
+----------------------+------------------+---------------------------------------+
| Minor                |  0               |                                       |   
+----------------------+------------------+---------------------------------------+
| Patch                |  0               |   Optional - if left off,             | 
|                      |                  |   patch value of 0 is                 |
|                      |                  |   dropped from output.                |
+----------------------+------------------+---------------------------------------+
| separator            |  . -             |   Optional. Dash or dot.              |
+----------------------+------------------+---------------------------------------+
| Pre-Release          |  r or R          |   r = short name 'a', 'b', etc.       |
|                      |                  |   R = long name 'alpha', 'beta', etc. |
+----------------------+------------------+---------------------------------------+
| separator            |  . -             |   Optional. Dash or dot.              |
+----------------------+------------------+---------------------------------------+
| Pre-Release Version  |  0               |   Optional. Pre-release version number|
|                      |                  |   will always display.                |
+----------------------+------------------+---------------------------------------+

Usage examples:

==========================  =====================
 dover command                      output
==========================  =====================
dover                       0.4.0-dev.1
dover --format 000-R.0      0.0.0-dev.1 
dover --format 000-R-0      0.4.0-dev-1
dover --format 00-R-0       0.4-dev-1
dover --format 00R          0.4dev1
dover --format 00r          0.4d1
dover --format 00.r         0.4.d1
dover --format 00r0         0.4d1
dover --format 00-r0        0.4-d1
dover --format 00-r-0       0.4-d-1
==========================  =====================


What If There Is a Problem?
^^^^^^^^^^^^^^^^^^^^^^^^^^^

If at any point the version numbers between files being tracked are
missaligned, ``dover`` will raise an error:

.. code:: text

    ... dover increment --major --apply
    Not all file versions match:

        setup.py      0.1.0  (__version__ = '0.1.0')
        setup.cfg     0.3.0  (version = 0.3.0)
        dover/cli.py  0.1.0  (__version__ = '0.1.0')


.. |version-badge| image:: https://img.shields.io/badge/version-v0.5.1-green.svg

.. |codacy-badge| image:: https://api.codacy.com/project/badge/Grade/b92162d5dce1431caac8dcece168b0f4
                  :target: https://www.codacy.com/app/bitbucket_9/dover?utm_source=mgemmill@bitbucket.org&amp;utm_medium=referral&amp;utm_content=mgemmill/dover&amp;utm_campaign=Badge_Grade

.. |codacy-coverage| image:: https://api.codacy.com/project/badge/Coverage/b92162d5dce1431caac8dcece168b0f4
                     :target: https://www.codacy.com/app/bitbucket_9/dover?utm_source=mgemmill@bitbucket.org&amp;utm_medium=referral&amp;utm_content=mgemmill/dover&amp;utm_campaign=Badge_Coverage


Indices and tables
==================

* :ref:`search`

