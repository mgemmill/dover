###### FOLLOW ALL STEPS #######

### Format yer code! ###
# Run black!

### Lint yer code! ###
# Run flake8!
# Run PyLint!

### Test yer code! ###
# Run pytest
# Run tox

### Bump yer version ###
# Run Dover!

### Commit yer changes! And push ###

### Send yer coverage to Cadacy ###
# Run coverage xml
# Run python-codacy-coverage -r coverage.xml

### Test publication ###
# twine upload --repository-url https://test.pypi.org/legacy/ dist/*

### Now, yer ready to publish this puppy! ###
# Run this script:
# . publish.sh

VERSION=`dover`

echo "#######################
echo "PUBLISHING DOVER v${VERSION}"
echo "#######################

. make_docs.sh build

# at this point, we should run `python setup.py check -r -s` to 
# be sure we have the proper formatting.
python setup.py check
python setup.py check --restructuredtext --strict
python setup.py check --metadata --strict


#
# create distributions
#
echo "##############################"
echo "CREATING DISTRIBUTION FOR ${VERSION}"
echo "##############################"
rm dist/*
python setup.py sdist bdist_wheel

#
# upload distributions
#
echo "##################################"
echo "UPLOADING DOVER ${VERSION} TO PYPI...."
echo "##################################"
# twine upload --repository-url https://test.pypi.org/legacy/ --config-file setup.cfg dist/*
twine upload --config-file setup.cfg dist/*

# twine upload --config-file setup.cfg dist/dover-${VERSION}.tar.gz
# twine upload --config-file setup.cfg dist/dover-${VERSION}-py3-none-any.whl
