History
^^^^^^^

0.5.0 (2018-11-25)
------------------

- added new dover config option for projects using pyproject.toml
- started use of `black` for code formating


0.4.0 (2018-03-10)
------------------

- Added `--dev` option as a pre-release state.
- Added `--release` option that clears any pre-release
  versioning.
- Added `--format` option that allows user to control the
  format from the standard `0.0.0-dev.1` format.


0.3.1 (2018-03-02)
------------------

- documentation update to deal with 
  invalid README formating.


0.3.0 (2018-03-02)
------------------

- added pre-release options
- added --no-list option
- expanded documentation
- 100% code coverage


0.2.1 (2018-02-22)
------------------

-  expanded tests
-  general code clean up


0.2.0 (2018-02-22)
------------------

-  improved alignment of output
-  added additional version matching check


0.1.0 (2018-02-18)
------------------

-  initial commit
